<?php

add_action( 'wp_enqueue_scripts', 'devit_scripts' );
function devit_scripts()
{
	wp_enqueue_style('style-css', get_stylesheet_uri());
	wp_enqueue_style('jquery-toast', get_stylesheet_directory_uri() . '/css/jquery.toast.min.css');
	wp_enqueue_style('devit-style-css', get_stylesheet_directory_uri() . '/css/style.css');

	wp_enqueue_script( 'jquery' );
	wp_enqueue_script('jquery-toast-js', get_stylesheet_directory_uri() . '/js/jquery.toast.min.js', array(), null, true);
	wp_enqueue_script('script-js', get_stylesheet_directory_uri() . '/js/script.js', array(), null, true);

}

add_action( 'after_setup_theme', 'devit_add_woocommerce_support' );
function devit_add_woocommerce_support() {
	add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'my_remove_product_result_count', 99 );
function my_remove_product_result_count() {
	remove_action( 'woocommerce_before_shop_loop' , 'woocommerce_result_count', 20 );
	remove_action( 'woocommerce_after_shop_loop' , 'woocommerce_result_count', 20 );
	remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
}
if ( ! current_user_can( 'manage_options' ) ) {
	show_admin_bar( false );
}

add_action( 'wp_enqueue_scripts', 'myajax_data', 99 );
function myajax_data() {

	wp_localize_script( 'script-js', 'myajax',
		array(
			'url' => admin_url('admin-ajax.php')
		)
	);
}

add_action('wp_ajax_woocommerce_ajax_add_to_cart', 'woocommerce_ajax_add_to_cart');
add_action('wp_ajax_nopriv_woocommerce_ajax_add_to_cart', 'woocommerce_ajax_add_to_cart');

function woocommerce_ajax_add_to_cart() {

	$product_id = apply_filters('woocommerce_add_to_cart_product_id', absint($_POST['product_id']));
	$quantity = empty($_POST['quantity']) ? 1 : wc_stock_amount($_POST['quantity']);
	$variation_id = absint($_POST['variation_id']);
	$passed_validation = apply_filters('woocommerce_add_to_cart_validation', true, $product_id, $quantity);
	$product_status = get_post_status($product_id);

	if ($passed_validation && WC()->cart->add_to_cart($product_id, $quantity, $variation_id) && 'publish' === $product_status) {

		do_action('woocommerce_ajax_added_to_cart', $product_id);

		if ('yes' === get_option('woocommerce_cart_redirect_after_add')) {
			wc_add_to_cart_message(array($product_id => $quantity), true);
		}

		WC_AJAX :: get_refreshed_fragments();
	} else {

		$data = array(
			'error' => true,
			'product_url' => apply_filters('woocommerce_cart_redirect_after_error', get_permalink($product_id), $product_id));

		echo wp_send_json($data);
	}

	wp_die();
}

if (function_exists('register_sidebar')){
	register_sidebar( array(
		'name'          => 'Shop', //название виджета в админ-панели
		'id'            => 'shop_sidebar', //идентификатор виджета
		'description'   => 'Filter Section', //описание виджета в админ-панели
		'before_widget' => '<div id="%1$s" class="widget %2$s">', //открывающий тег виджета с динамичным идентификатором
		'after_widget'  => '<div class="clear"></div></div>', //закрывающий тег виджета с очищающим блоком
		'before_title'  => '<span class="widget-title">', //открывающий тег заголовка виджета
		'after_title'   => '</span>',//закрывающий тег заголовка виджета
	) );
}

add_action('woocommerce_before_shop_loop', 'devit_register_widget');
function devit_register_widget() {
	echo '<div class="my-filter-ajax">';
	dynamic_sidebar( 'shop_sidebar' );
	echo '</div>';
}

remove_action( 'woocommerce_after_shop_loop', 'woocommerce_pagination', 10 );

add_action('wp_ajax_devit_filter_product', 'filter_products');
add_action('wp_ajax_nopriv_devit_filter_product', 'filter_products');
function filter_products(){

	//$select_opc = $_POST['opc']; //here is your post data, you can use its value to filter the products
	$min_price = $_GET['min_price'];
	$max_price = $_GET['max_price'];
	$color = $_GET['color'];
	$size = $_GET['size'];
	$tax_color = '';
	$tax_size = '';
	$tax_query = array();
	//echo !empty( $min_price );
	if ( isset( $min_price ) && isset($max_price)) {
		$meta_price = array(
			'key' => '_price',
			'value' => $min_price,
			'type' => 'numeric',
			'compare' => '>='
		);
		$meta_price2 = array(
			'key' => '_price',
			'value' => $max_price,
			'type' => 'numeric',
			'compare' => '<='
		);
	}

	if (!empty($color)) {
		$tax_color = array(
			'taxonomy' => 'pa_color',
			'field' => 'slug',
			'terms'    => $color
		);
	}

	if (!empty($size)) {
		$tax_size = array(
			'taxonomy' => 'pa_size',
			'field' => 'slug',
			'terms'    => $size,
			'operator' => 'AND'
		);
	}

	if ( !empty($color) || !empty($size) ) {
		$tax_query = array(
			'relation' => 'AND',
			$tax_color,
			$tax_size,
		);
	}

	$args = array(
		'post_type' => array('product'), //'product_variation'
		'posts_per_page' => 30,
		'orderby' => 'title',
		'order' => 'ASC',
		//'paged'          => $paged,
		'meta_query' => array(
			'relation' => 'AND',
			$meta_price,
			$meta_price2
		),
		'tax_query' => $tax_query
	);

	$products = '';
	$loop = new WP_Query( $args );

	if ( $loop->have_posts() ) {
		while ( $loop->have_posts() ) : $loop->the_post();
			$products .= wc_get_template_part( 'content', 'product' );
		endwhile;

		wp_reset_postdata();

	} else {
		$products = 'No products found';
	}
	echo $products;
	exit;
}

add_filter( 'woocommerce_product_search_filter_product_loop_content', 'filter_function_name_4141' );
function filter_function_name_4141( $html ){
	echo $html;
	return $html;
}
