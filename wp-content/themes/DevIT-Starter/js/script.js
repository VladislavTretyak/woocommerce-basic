(function ($) {

    $(document).on('click', '.single_add_to_cart_button', function (e) {
        e.preventDefault();

        var $thisbutton = $(this),
            $form = $thisbutton.closest('form.cart'),
            id = $thisbutton.val(),
            product_qty = $form.find('input[name=quantity]').val() || 1,
            product_id = $form.find('input[name=product_id]').val() || id,
            variation_id = $form.find('input[name=variation_id]').val() || 0;

        var data = {
            action: 'woocommerce_ajax_add_to_cart',
            product_id: product_id,
            product_sku: '',
            quantity: product_qty,
            variation_id: variation_id,
        };

        $(document.body).trigger('adding_to_cart', [$thisbutton, data]);

        $.ajax({
            type: 'post',
            url: wc_add_to_cart_params.ajax_url,
            data: data,
            beforeSend: function (response) {
                $thisbutton.removeClass('added').addClass('loading');
            },
            complete: function (response) {
                $thisbutton.addClass('added').removeClass('loading');
            },
            success: function (response) {
                if (response.error && response.product_url) {
                    window.location = response.product_url;
                    return;
                } else {
                   // alert(product_qty);
                    if (product_qty == 1) {
                        massage = 'You have added ' + product_qty + ' item to your cart';
                    } else {
                        massage = 'You have added ' + product_qty + ' items to your cart';
                    }
                    $.toast({
                        text: massage, // Text that is to be shown in the toast
                        heading: 'Success', // Optional heading to be shown on the toast
                        icon: 'success', // Type of toast icon
                        showHideTransition: 'fade', // fade, slide or plain
                        allowToastClose: true, // Boolean value true or false
                        hideAfter: 3000, // false to make it sticky or number representing the miliseconds as time after which toast needs to be hidden
                        stack: 5, // false if there should be only one toast at a time or a number representing the maximum number of toasts to be shown at a time
                        position: 'top-center', // bottom-left or bottom-right or bottom-center or top-left or top-right or top-center or mid-center or an object representing the left, right, top, bottom values



                        textAlign: 'left',  // Text alignment i.e. left, right or center
                        loader: true,  // Whether to show loader or not. True by default
                        loaderBg: '#9EC600',  // Background color of the toast loader
                        beforeShow: function () {}, // will be triggered before the toast is shown
                        afterShown: function () {}, // will be triggered after the toat has been shown
                        beforeHide: function () {}, // will be triggered before the toast gets hidden
                        afterHidden: function () {}  // will be triggered after the toast has been hidden
                    });
                    //$(document.body).trigger('added_to_cart', [response.fragments, response.cart_hash, $thisbutton]);
                }
            },
        });

        return false;
    });
    jQuery('#woocommerce_layered_nav-2 > form, #woocommerce_layered_nav-3 form').on('submit', function(e) {
        e.preventDefault();
        e.stopPropagation();
        return false;
    });

    $('#woocommerce_price_filter-4, .price_slider').on("click", devit_ajax);
    $('#woocommerce_layered_nav-2').on("change", devit_ajax);
    $('#woocommerce_layered_nav-3').on("change", devit_ajax);
//    $(window).on("load", devit_localstorege);
    //$(window).on("load", devit_ajax);

    // function devit_localstorege() {
    //     $('#min_price').val(localStorage.min_price);
    //     $('#max_price').val(localStorage.max_price);
    //     $('.dropdown_layered_nav_size').val(localStorage.size);
    //     $('.dropdown_layered_nav_color').val(localStorage.color);
    // }

    function devit_ajax(e) {
        e.preventDefault();
        min_price = $('#min_price').val();
        max_price = $('#max_price').val();
        size = $('.dropdown_layered_nav_size').val();
        color = $('.dropdown_layered_nav_color').val();
        //var currentLocation = window.location;
        history.pushState({}, '', '?post_type=product&filter_color=' + color + '&filter_size=' + size + '&max_price=' + max_price + '&min_price=' + min_price + '');

        console.log(size, color, min_price, max_price);
        var data = {
            action: 'devit_filter_product',
            min_price: min_price,
            max_price: max_price,
            size: size,
            color: color
        };

        // 'ajaxurl' не определена во фронте, поэтому мы добавили её аналог с помощью wp_localize_script()
        jQuery.get( myajax.url, data, function(response) {
            //alert('Получено с сервера: ' + response);
            $('.products').html(response);
            // localStorage.setItem('min_price', min_price);
            // localStorage.setItem('max_price', max_price);
            // localStorage.setItem('size', size);
            // localStorage.setItem('color', color);
        });
}

 })(jQuery);
