Stripe.setPublishableKey(stripe_vars.publishable_key);
function stripeResponseHandler(status, response) {
    if (response.error) {
        // show errors returned by Stripe
        jQuery(".woocommerce-error").append("<li>" + response.error.message + "</li>");
        // re-enable the submit button
        jQuery('#stripe-submit').attr("disabled", false);
    } else {
        var form$ = jQuery(".woocommerce-checkout");
        // token contains id, last4, and card type
        var token = response['id'];
        console.log('token', token);
        // insert the token into the form so it gets submitted to the server
        form$.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
        // and submit
        //console.log(form$.get(0));
        form$.submit();
    }
}
jQuery(document).ready(function($) {
    $(".woocommerce-checkout").on('submit', function(event) {
        event.preventDefault();
        // disable the submit button to prevent repeated clicks
        alert($('.card-number').val());
        $('#stripe-submit').attr("disabled", "disabled");

        // send the card details to Stripe
        Stripe.createToken({
            number: $('.card-number').val(),
            cvc: $('.card-cvc').val(),
            exp_month: $('.card-expiry-month').val(),
            exp_year: $('.card-expiry-year').val()
        }, stripeResponseHandler);

        // prevent the form from submitting with the default action
        return false;
    });
});