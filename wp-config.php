<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'wp_database' );

/** Имя пользователя MySQL */
define( 'DB_USER', 'pmauser' );

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', 'ZuFpWKsQb1' );

/** Имя сервера MySQL */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ',5Vy@:pQv1m#{*%=7*Y|WpjA!xAU-BllyOzjD%X{Dc;)}VjpLwHA;=6Y]0MhuB6B' );
define( 'SECURE_AUTH_KEY',  'Q)/[Q/j8_$_%9c-V~nlCtW[#Na!yS`:fR6b%lavW)VTYklyR<xqT%pqckHyN(rMZ' );
define( 'LOGGED_IN_KEY',    'yjMF$V{ Y(nCRbeaO]I_EBri=h.}^2^D4vd0vBF/XX)gdKtUn<2GP-yKHoo$aOM0' );
define( 'NONCE_KEY',        'D<%4XA?9lg:,f)`gKs6kw0^.0+ )huVz*~Owa~TKXvb@uB]Ivl>H.d7|< ]pn*Mc' );
define( 'AUTH_SALT',        'rc!t6HC-Bn%8aabyr|XAM},E8)0$Gl{9_#rOlH~Box8ILg[[EDTx5ZmWbZv,RD&e' );
define( 'SECURE_AUTH_SALT', 'L>}D50fv^!maVT Y ofG3BHgL{#8oT}>/lv9,F|NXAze$TDKwP)pDRg#SpkDss8o' );
define( 'LOGGED_IN_SALT',   '#*o:SE+LXbJgd]>wHuK#+?mBbugH?7|$*``.@f&B4`B$#fm.7SgeKO1$A!2nETR2' );
define( 'NONCE_SALT',       'EN-:XLsW}A~-Y/pf$:aT-3$Q^(/XlG-sT5yCCCa4vwsnAb(,avLwb)??ga%J w{&' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */

/* Blocked loading mixed active content fix*/
$_SERVER['HTTPS'] = 'on';
define( 'WP_DEBUG', true );

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once( ABSPATH . 'wp-settings.php' );